export const dataUsers = [
  {
    id: 1,
    isYou: true,
    isPremium: false,
    isOnline: true,
    nickname: "JackAttack123",
    firstname: "John",
    lastname: "JoKes",
    country: "Trinidad and Tobago",
    countryFlag: "tt",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_5.jpg",
    email: "jackjack@gmail.com",
    clan: 1,
    bio: "Hi, I'm Jack, I love to play VALORANT & CS:GO with my friends. I was born in 1976 but I move my mouse quicker than anyone. I have 7 children that I named based on my favorite games: Link, Mario, Zelda, Kane, Fox, Raiden and Valentine.",
    socials: {
      riotNickname: "JackAttack123",
      riotHashtag: 8573,
      twitch: "@jackattack123"
    },
    stats: {
      xp: 10000,
      playedBrawls: 143,
      playedTournaments: 12,
      gamestats: [
        {
          game: 1,
          winrate: "25%",
          kdratio: "0.81",
          headshotkillsavg: "1.14",
          assistavg: "2.3"
        },
        {
          game: 2,
          winrate: "40%",
          kdratio: "1.4",
          headshotkillsavg: "1.87",
          assistavg: "2.3"
        }
      ]
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      1,
      4
    ]
  },
  {
    id: 2,
    isYou: false,
    isPremium: true,
    isOnline: false,
    nickname: "Kes2Band",
    firstname: "Kees",
    lastname: "Dieffenthaller",
    country: "Germany",
    countryFlag: "de",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_1.jpg",
    email: "kes@gmail.com",
    clan: null,
    bio: "Call me Kees. I'm a Tv geek. Student. Web fan. Organizer. Coffee ninja. Zombie scholar. Communicator. Twitter fanatic.",
    socials: {
      riotNickname: "KeesDief",
      riotHashtag: 8953,
      twitch: "@KeesDief"
    },
    stats: {
      xp: 234,
      playedBrawls: 34,
      playedTournaments: 0,
      gamestats: [
        {
          game: 2,
          winrate: "25%",
          kdratio: "0.81",
          headshotkillsavg: "1.14",
          assistavg: "2.3"
        },
        {
          game: 3,
          winrate: "40%",
          kdratio: "1.4",
          headshotkillsavg: "1.87",
          assistavg: "2.3"
        }
      ]
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      1,
      2,
      5
    ]
  },
  {
    id: 3,
    isYou: false,
    isPremium: false,
    isOnline: true,
    nickname: "MrKilla",
    firstname: "Johna",
    lastname: "Mapp",
    country: "Grenada",
    countryFlag: "gd",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_2.jpg",
    email: "mrkil@gmail.com",
    clan: 3,
    bio: "Extreme alcohol guru. Hardcore troublemaker. Food lover. Musicaholic. Typical social media geek.",
    socials: {
      riotNickname: "MrKilla",
      riotHashtag: 8573,
      twitch: "@misterkilla"
    },
    stats: {
      xp: 9275,
      playedBrawls: 243,
      playedTournaments: 22
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      1
    ]
  },
  {
    id: 4,
    isYou: false,
    isPremium: false,
    isOnline: true,
    nickname: "Nomadiction",
    firstname: "Kyriakos",
    lastname: "Mitsotakis",
    country: "Greece",
    countryFlag: "gr",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_3.jpg",
    email: "kykymimi@gmail.com",
    clan: 2,
    bio: "Do not disturb, I am Gaming.",
    socials: {
      riotNickname: "kykymimiGr",
      riotHashtag: 8573,
      twitch: "@kykymimi"
    },
    stats: {
      xp: 18970,
      playedBrawls: 143,
      playedTournaments: 12,
      gamestats: [
        {
          game: 1,
          winrate: "25%",
          kdratio: "0.81",
          headshotkillsavg: "1.14",
          assistavg: "2.3"
        }
      ]
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      5
    ]
  },
  {
    id: 5,
    isYou: false,
    isPremium: false,
    isOnline: false,
    nickname: "Johnny",
    firstname: "Johnny",
    lastname: "Deep",
    country: "United States",
    countryFlag: "us",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_4.jpg",
    email: "dirtyjohnny@gmail.com",
    clan: null,
    bio: "Nothing epic happens in real life, so I chose to be a gamer.",
    socials: {
      riotNickname: "DirtyJohnny",
      riotHashtag: 8573,
      twitch: "@dirtyjohnny"
    },
    stats: {
      xp: 874,
      playedBrawls: 3,
      playedTournaments: 0,
      gamestats: [
        {
          game: 2,
          winrate: "25%",
          kdratio: "0.81",
          headshotkillsavg: "1.14",
          assistavg: "2.3"
        },
        {
          game: 3,
          winrate: "40%",
          kdratio: "1.4",
          headshotkillsavg: "1.87",
          assistavg: "2.3"
        }
      ]
    },
    wallet: {
      coins: 532,
      tokens: 759,
      tickets: 3
    },
    games: [
      1,
      2
    ]
  },
  {
    id: 6,
    isYou: false,
    isPremium: false,
    isOnline: false,
    nickname: "PopoRob",
    firstname: "Rob",
    lastname: "Williams",
    country: "United Kingdom",
    countryFlag: "gb",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_6.jpg",
    email: "robbiewilliams@gmail.com",
    clan: 5,
    bio: "Hi everyone! My name is PopoRob, but around the Internet I go by Zentouro (don’t ask, its a weird story involving an elven name generator when I was 8). I’m an Environmental Studies major at Middlebury College, focusing in Film and Media Culture. I also have minors in Chinese and Computer Science.",
    socials: {
      riotNickname: "PopoRob",
      riotHashtag: 8573,
      twitch: "@poporob"
    },
    stats: {
      xp: 239,
      playedBrawls: 74,
      playedTournaments: 2
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      1
    ]
  },
  {
    id: 7,
    isYou: false,
    isPremium: false,
    isOnline: false,
    nickname: "Valance",
    firstname: "Valérie",
    lastname: "Williams",
    country: "France",
    countryFlag: "fr",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_7.jpg",
    email: "valance@gmail.com",
    clan: 4,
    bio: "Gamer. Entrepreneur. Analyst. Hardcore problem solver. Alcohol evangelist. Internet aficionado. Food specialist.",
    socials: {
      riotNickname: "valance",
      riotHashtag: 1425,
      twitch: "@valance"
    },
    stats: {
      xp: 198,
      playedBrawls: 15,
      playedTournaments: 0
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      7
    ]
  },
  {
    id: 8,
    isYou: false,
    isPremium: true,
    isOnline: false,
    nickname: "Pilar",
    firstname: "Steven",
    lastname: "Catterpilar",
    country: "United Kingdom",
    countryFlag: "gb",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_8.jpg",
    email: "PilarPilar@gmail.com",
    clan: 4,
    socials: {
      riotNickname: "Pilar",
      riotHashtag: 7482,
      twitch: "@PilarPilar"
    },
    stats: {
      xp: 244,
      playedBrawls: 54,
      playedTournaments: 23
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      4,
      6
    ]
  },
  {
    id: 9,
    isYou: false,
    isPremium: false,
    isOnline: true,
    nickname: "HansGrubby",
    firstname: "Hans",
    lastname: "Grubber",
    country: "Germany",
    countryFlag: "de",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_9.jpg",
    email: "hanshans@gmail.com",
    clan: 6,
    bio: "I'm an unapologetic gamer. Social media enthusiast. Student. Certified alcohol guru. Problem solver. Hardcore twitter maven. Tv evangelist.",
    socials: {
      riotNickname: "HansGrubby",
      riotHashtag: 1254,
      twitch: "@HansGrubby"
    },
    stats: {
      xp: 10000,
      playedBrawls: 54,
      playedTournaments: 23
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      1,
      2,
      3,
      4
    ]
  },
  {
    id: 10,
    isYou: false,
    isPremium: false,
    isOnline: true,
    nickname: "FrenzyMan",
    firstname: "Scott",
    lastname: "Fritz",
    country: "Austria",
    countryFlag: "at",
    lang: "GER",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_10.jpg",
    email: "frenzyman@gmail.com",
    clan: null,
    bio: "Just one more game.",
    socials: {
      riotNickname: "FrenzyMan",
      riotHashtag: 1254,
      twitch: "@FrenzyMan"
    },
    stats: {
      xp: 297,
      playedBrawls: 2,
      playedTournaments: 0
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      1,
      7,
      3,
      4
    ]
  },
  {
    id: 11,
    isYou: false,
    isPremium: true,
    isOnline: true,
    nickname: "Jamlog",
    firstname: "Jamie",
    lastname: "DS",
    country: "Malta",
    countryFlag: "mt",
    lang: "ENG",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1655292255/Marketing/2022_prototype/DummyContent/avatars/avatar_user_11.jpg",
    email: "jamlog@gmail.com",
    clan: null,
    bio: "Sleep. Eat. Game. Repeat.",
    socials: {
      riotNickname: "Jamlog",
      riotHashtag: 1254,
      twitch: "@Jamlog"
    },
    stats: {
      xp: 297,
      playedBrawls: 2,
      playedTournaments: 0
    },
    wallet: {
      coins: 50000,
      tokens: 245,
      tickets: 3
    },
    games: [
      1,
      7,
      3,
      4
    ]
  }
]

export default {
  dataUsers,
}