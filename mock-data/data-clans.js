export const dataClans = [
  {
    id: 1,
    isYou: true,
    isPublic: true,
    hasInvitedYou: false,
    isFeatured: true,
    nickname: "Raccoon 5",
    tag: "RAC",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063975/Marketing/2022_prototype/DummyContent/teams/teamlogo_Foxy.png",
    bio: "We're a mostly Canadian clan playing League of Legends & DOTA2.French speakers are OK, but a lot of the chat will happen in English, so you know. And for the record, don't bother even joining if you're a noob ;-). So embrace your self for some real and seriouse competition to be player one",
    lang: "English",
    games: [
      2,
      4,
      3,
      1
    ],
    social: {
      twitch: "qdqsd",
      discord: "qsdqsd",
      youtube: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 1,
    members: [
      2,
      1,
      3
    ]
  },
  {
    id: 2,
    isYou: false,
    isPublic: true,
    hasInvitedYou: false,
    isFeatured: true,
    nickname: "PizzaYOLO",
    tag: "YOLO",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063975/Marketing/2022_prototype/DummyContent/teams/teamlogo_Gladion2.png",
    bio: "Siamo un clan prevalentemente italiano che gioca a League of Legends e DOTA2. Chi parla inglese va bene, ma gran parte delle chat avverrà in italiano, quindi lo sai ;-) Siamo abbastanza bravi con 4 giocatori Ultimate in LoL, ma siamo anche amichevoli con i noob!",
    lang: "Italian",
    games: [
      1,
      2
    ],
    social: {
      discord: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 4,
    members: [
      4,
      6,
      7,
      8
    ]
  },
  {
    id: 3,
    isYou: false,
    isPublic: false,
    hasInvitedYou: true,
    isFeatured: false,
    nickname: "BBL Esports",
    tag: "BBL",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063975/Marketing/2022_prototype/DummyContent/teams/teamlogo_Un1c0rns.png",
    bio: "We're a mostly Canadian clan playing League of Legends & DOTA2.French speakers are OK, but a lot of the chat will happen in English, so you know. And for the record, don't bother even joining if you're a noob ;-). So embrace your self for some real and seriouse competition to be player one",
    lang: "English",
    games: [
      2,
      4,
      3,
      1
    ],
    social: {
      twitch: "qdqsd",
      discord: "qsdqsd",
      youtube: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 1,
    members: [
      2,
      1,
      3
    ]
  },
  {
    id: 4,
    isYou: false,
    isPublic: false,
    hasInvitedYou: false,
    isFeatured: false,
    nickname: "Gorillas Esports",
    tag: "GOR",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063976/Marketing/2022_prototype/DummyContent/teams/teamlogo_GhOOOsTS.png",
    bio: "We're a mostly Canadian clan playing League of Legends & DOTA2.French speakers are OK, but a lot of the chat will happen in English, so you know. And for the record, don't bother even joining if you're a noob ;-). So embrace your self for some real and seriouse competition to be player one",
    lang: "English",
    games: [
      2,
      4,
      3,
      1
    ],
    social: {
      twitch: "qdqsd",
      discord: "qsdqsd",
      youtube: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 1,
    members: [
      4,
      6,
      7
    ]
  },
  {
    id: 5,
    isYou: false,
    isPublic: false,
    hasInvitedYou: false,
    isFeatured: false,
    nickname: "ACEND",
    tag: "AC",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063976/Marketing/2022_prototype/DummyContent/teams/teamlogo_DeerGamer.png",
    bio: "We're a mostly Canadian clan playing League of Legends & DOTA2.French speakers are OK, but a lot of the chat will happen in English, so you know. And for the record, don't bother even joining if you're a noob ;-). So embrace your self for some real and seriouse competition to be player one",
    lang: "English",
    games: [
      2,
      4,
      3,
      1
    ],
    social: {
      twitch: "qdqsd",
      discord: "qsdqsd",
      youtube: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 1,
    members: [
      1,
      7,
      9
    ]
  },
  {
    id: 6,
    isYou: false,
    isPublic: true,
    hasInvitedYou: false,
    isFeatured: false,
    nickname: "Dusty Smokers",
    tag: "DST",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063976/Marketing/2022_prototype/DummyContent/teams/teamlogo_Bunny_Hunger.png",
    bio: "We're a mostly Canadian clan playing League of Legends & DOTA2.French speakers are OK, but a lot of the chat will happen in English, so you know. And for the record, don't bother even joining if you're a noob ;-). So embrace your self for some real and seriouse competition to be player one",
    lang: "English",
    games: [
      2,
      4,
      3,
      1
    ],
    social: {
      twitch: "qdqsd",
      discord: "qsdqsd",
      youtube: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 1,
    members: [
      2,
      1,
      3
    ]
  },
  {
    id: 7,
    isYou: false,
    isPublic: true,
    hasInvitedYou: true,
    isFeatured: false,
    nickname: "Valorians",
    tag: "VAL",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063976/Marketing/2022_prototype/DummyContent/teams/teamlogo_AngryBeavers.png",
    bio: "We're a mostly Canadian clan playing League of Legends & DOTA2.French speakers are OK, but a lot of the chat will happen in English, so you know. And for the record, don't bother even joining if you're a noob ;-). So embrace your self for some real and seriouse competition to be player one",
    lang: "English",
    games: [
      2,
      4,
      3,
      1
    ],
    social: {
      twitch: "qdqsd",
      discord: "qsdqsd",
      youtube: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 1,
    members: [
      2,
      1,
      3
    ]
  },
  {
    id: 8,
    isYou: false,
    isPublic: false,
    hasInvitedYou: true,
    isFeatured: false,
    nickname: "#1",
    tag: "ONE",
    avatar: "https://res.cloudinary.com/gloot/image/upload/v1654063976/Marketing/2022_prototype/DummyContent/teams/teamlogo_CockyStars.png",
    bio: "We're a mostly Canadian clan playing League of Legends & DOTA2.French speakers are OK, but a lot of the chat will happen in English, so you know. And for the record, don't bother even joining if you're a noob ;-). So embrace your self for some real and seriouse competition to be player one",
    lang: "English",
    games: [
      2,
      4,
      3,
      1
    ],
    social: {
      twitch: "qdqsd",
      discord: "qsdqsd",
      youtube: "qsdqsd",
      twitter: "qsdqsd"
    },
    admin: 1,
    members: [
      2,
      1,
      3
    ]
  }
]

export default {
  dataClans,
}