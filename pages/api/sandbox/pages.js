export const SBpages = [
  {
    label: "About",
    icon: "",
    pages: [
      {
        label: "About",
        tags: "framework,about,css,gettingstarted,components,styled,system,design,tailwindcss,reactjs",
        url: "about",
      },
    ],
  },
  {
    label: "Exercises",
    icon: "",
    pages: [
      {
        label: "Exercise #1",
        tags: "basics,colors,background,text,main,ui,main,accent,gradient,colour",
        url: "exercise-1",
      },
    ],
  },
];

export default {
  SBpages,
}